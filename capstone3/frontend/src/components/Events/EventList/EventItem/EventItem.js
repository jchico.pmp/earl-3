import React from 'react';

import './EventItem.css';

const eventItem = props => (
  <li key={props.eventId} className="events__list-item">
    <div>
      <h1>{props.title}</h1>
      {props.userId === props.creatorId ? <p>You're the publisher</p> : null}
      <h2>
        ₱{props.price} - {
          new Date(props.date).toLocaleString('default', { month: 'long' }) + " " + new Date(props.date).getDate().toString() + ", " + new Date(props.date).getUTCFullYear().toString()
        }
      </h2>
    </div>
    <div>
      {props.userId !== props.creatorId ?
        <button className="btn" onClick={props.onDetail.bind(this, props.eventId)}>
        View Details
      </button> : null
      }
        
    </div>
  </li>
);

export default eventItem;
