import React, {useState} from 'react';
import StripeCheckout from 'react-stripe-checkout';
import axios from 'axios';
import './BookingList.css';

const bookingList = (props) => {
  const [event] = React.useState({
    name: "Event",
    price: 825
  });
  
  function handleToken(token){
    axios.post('https://codesandbox.io/s/node-stripe-checkout-final-m3vw8/checkout',{
      token,
      event
    })
  }

  return(
    <ul className="bookings__list">
    {props.bookings.map(booking => {
      return (
        <li key={booking._id} className="bookings__item">
          <div className="bookings__item-data">
            {booking.event.title} -{' '}
            {new Date(booking.createdAt).toLocaleDateString()}
          </div>
          <div className="bookings__item-actions">
            <StripeCheckout className="btn"
            stripeKey="pk_test_WvXiIuhbvm3a98CvserW9fBr00NwccNcs4"
            token={handleToken}
            billingAddress
            shippingAddress
            amount={event.price * 100}
            name={event.name}
            />
            <button className="btn" onClick={props.onDelete.bind(this, booking._id)}>Cancel</button>
          </div>
        </li>
      );
    })}
  </ul>
  )
  
};

export default bookingList;
