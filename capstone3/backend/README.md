# Eventually

Eventually is a MERNG Stack web app created by Earl Diaz.

## Installation

After cloning

```bash
npm install
```
at root directory then
```bash
cd frontend
npm install
```
Afterwards, open up this link for Stripe to work: [Stripe Backend](https://codesandbox.io/s/node-stripe-checkout-final-m3vw8)
paste this secret key if needed: sk_test_20Bv1b2lvZanXXlrL975gCmZ00njVSaARF

then
```bash
npm start
```
both root directory and frontend directory