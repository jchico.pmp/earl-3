@if(Session::has('success'))
<div class="col-12">
    <div class="alert alert-success text-center">
        {{Session::get('success')}}
    </div>
</div>
@endif

@if(Session::has('fail'))
<div class="col-12">
    <div class="alert alert-danger text-center">
        {{Session::get('fail')}}
    </div>
</div>
@endif

@if($errors->any())
<div class="col-12">
    <div class="alert alert-danger">
        <ul class="text-left mb-0">
            @foreach($errors->all() as $error)
                <li>{{$error}}</li>
            @endforeach
        </ul>
    </div>
</div>
@endif