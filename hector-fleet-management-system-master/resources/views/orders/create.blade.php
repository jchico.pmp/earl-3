@extends('layouts.app')

@section('content')

<!-- BANNER -->

{{-- Start of Messages --}}
@include('messages')
{{-- End of Messages --}}

<div class="row px-3">
	<div class="col-12 text-center">
	    <h1>Create Order</h1>
	</div>
</div>

<!-- MAIN CONTENT -->
<div class="row section">
	
	<div class="col-sm-7 mb-3">
		<div class="card">
			<div class="card-header">
				Fill-up details
			</div>
			<div class="card-body">
				<form action="#">
					<div class="form-group">
						<label for="borrow-date">
							Borrow Date:
						</label>
						<input type="date" class="form-control" name="borrow-date">
					</div>
					<div class="form-group">
						<label for="return-date">
							Return Date:
						</label>
						<input type="date" class="form-control" name="return-date">
					</div>
					<div class="form-group">
						<label for="purpose">Purpose:</label>
						<textarea name="purpose" id="purpose" rows="5" class="form-control"></textarea>
					</div>

					<div class="col-sm-6 col-lg-4 mx-auto">
						<button class="btn btn-primary w-100">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="col-sm-5 mb-3">
		
	</div>

</div>

@endsection