@extends('layouts/app')

@section('title', $vehicle->plate_number)

@section('content')
<?php $vehicle_model = $vehicle->vehicle_model ?>

{{-- Start of Messages --}}
<div class="row">
    @include('messages')
</div>
{{-- End of Messages --}}

{{-- Car Details --}}
<div class="card mb-3">
    <div class="card-header order">
        <div class="row px-3">
            <a href="{{URL::previous()}}" class="btn btn-primary mr-3">Back</a>
            <h2 class="m-0">Vehicle Details</h2>
        </div>
    </div>

    <div class="card-body bordered p-5">
        <div class="row">
            <div class="col-lg-6">
                <img src="/public/{{$vehicle_model->image}}" alt="" class="img-fluid w-100">
            </div>  
        
            <div class="col-lg-6">
                <h3>{{$vehicle->plate_number}} <span class="badge
                    @if($vehicle->vehicle_status_id == 1)
                        bg-success
                    @elseif($vehicle->vehicle_status_id == 2)
                        bg-danger
                    @else
                        bg-grey
                    @endif
                    text-light px-3">{{$vehicle->vehicle_status->name}}</span></h3>
                <a href="{{ route('vehicle_models.show', ['vehicle_model' => $vehicle_model->id]) }}"><h5 class="mb-0">{{strtoupper($vehicle_model->make . " " . $vehicle_model->name)}} <small>{{ $vehicle_model->year }}</small></h5></a>
                <p class="mb-5">{{$vehicle_model->vehicle_category->name}} | Seats: {{$vehicle_model->seats}}</p>

                @can('isMod')
                <div class="col-12 pl-0 mt-5">
                    <form action="{{ route('vehicles.update', ['vehicle' => $vehicle->id]) }}" method="post">
                        @csrf
                        @method('put')
                        <h5>Status:</h5>
                        <div class="form-check form-check-inline mb-3">
                            {{-- START STATUS ITEMS --}}
                            @foreach($vehicle_statuses as $status)
                            <input type="radio" class="form-check-input" name="status" id="{{ $status->name }}" value="{{ $status->id }}" {{ $status->id == $vehicle->vehicle_status->id ? "checked" : "" }}>
                                <label class="form-check-label mr-4" for="status-STATUS">{{ $status->name }}</label>
                            @endforeach
                            {{-- END STATUS ITEMS --}}
                        </div>
                        <button class="btn btn-primary ml-auto">Change Status</button>
                    </form>
                </div>
                @endcan

            </div>
        </div>
    </div>

    <div class="card-footer order text-right">
        <a href="{{route('vehicles.edit', ['vehicle' => $vehicle->id])}}" class="btn btn-secondary text-light">UPDATE VEHICLE</a>
        <form action="{{route('vehicles.destroy', ['vehicle' => $vehicle->id])}}" method="post" class="d-inline">
            @csrf
            @method('delete')
            <button class="btn btn-danger w-lg-25" type="submit">Delete Vehicle</button>
        </form>
    </div>

</div>

{{-- Start Car Orders --}}
<div class="card">

    <div class="card-header order">
        <h3 class="mb-0">Vehicle Orders</h3>
    </div>

    <div class="card-body px-0 py-0 bordered">
        <table class="table table-hover text-center mb-0">
            @if($vehicle->orders->first())
            <thead>
                <tr>
                    <th>Requested by</th>
                    <th>Order Number</th>
                    <th>Order Date</th>
                    <th>Borrow Date</th>
                    <th>Return Date</th>
                    <th>Order Details</th>
                </tr>
            </thead>
            <tbody>
                {{-- Start of Order List --}}
                @foreach($vehicle->orders as $order)
                @if(Auth::user()->can('isMod') || $order->user_id === Auth::user()->id)
                <tr>
                    <td>{{ ucwords($order->user->firstname . " " . $order->user->lastname) }}</td>
                    <td>{{ $order->order_number }}</td>
                    <td>{{ $order->created_at }}</td>
                    <td>{{ $order->borrow_date }}</td>
                    <td>{{ $order->return_date }}</td>
                    <td>
                        <a href="{{route('orders.show', ['order' => $order->id])}}" class="btn btn-sm btn-secondary">View Details</a>
                    </td>
                </tr>
                @endif
                @endforeach
                {{-- End of Order List --}}
                @else
                <div class="col-12 text-center py-5">
                    <h5 class="mb-0">This vehicles has no orders</h5>
                </div>
                @endif
            </tbody>
        </table>
    </div>

    <div class="card-footer order">

    </div>
</div>
{{-- End of Car Orders --}}


@endsection