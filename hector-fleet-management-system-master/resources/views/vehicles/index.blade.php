@extends('layouts/app')

@section('title', 'Vehicles')

@section('content')

<div class="row px-1 mt-4 mb-0">
    
{{-- Start of Messages --}}
@include('messages')
{{-- End of Messages --}}

    <div class="col-12">
        <div class="row">
            <div class="col-6">
                <h1> {{ Auth::user()->can('isUser')? "Approved Vehicles" : "Vehicles" }} </h1>
            </div>
        
            @can('isAdmin')
            <div class="col-6 text-right">
                <a href="{{route('vehicles.create')}}" class="btn btn-secondary ml-1 mb-2">&plus; Add a Vehicle</a>
            </div>
            @endcan
        </div>
    </div>

    
    {{-- Start of Vehicle Cards --}}
    @foreach($vehicles as $vehicle)
        @include('vehicles.inc.vehicle_card')
    @endforeach
    {{-- End of Vehicle Cards --}}

</div>

@endsection