@extends('layouts/app')

@section('title', 'Add a Vehicle')

@section('content')

<div class="row mt-4">

    {{-- Start of Messages --}}
    @include('messages')
    {{-- End of Messages --}}

    <div class="col-12 text-center">
        <h1>Add New Vehicle</h1>
    </div>

    <div class="col-12 col-md-8 col-lg-6 mx-auto mt-5 text-center">
        <form action="{{route('vehicles.store')}}" method="post">
            @csrf
            
            <div class="form-group text-left">
                <label for="plate_number">Plate Number:</label>
                <input type="text" name="plate_number" class="form-control" value="{{old('plate_number')}}" required>
            </div>
            <div class="form-group text-left mb-1">
                <label for="model">Model:</label>
                <select name="model" class="form-control mb-2" required>
                    <option value="" selected>Choose vehicle model</option>
                    {{-- Start of Model List --}}
                    @foreach($vehicle_models as $vehicle_model)
                    <option value="{{$vehicle_model->id}}"
                        {{old('model') == $vehicle_model->id ? "selected" : ""}}
                        >
                        {{"$vehicle_model->make $vehicle_model->name $vehicle_model->year"}}
                    </option>
                    @endforeach
                    {{-- End of Model List --}}

                </select>
                <p class="text-center mb-5">
                    Vehicle model not on the list? 
                    <a href="#" class="btn btn-sm btn-secondary ml-2">Add New Vehicle Model</a>
                </p>
            </div>
            <button class="btn btn-primary px-5 w-100 mb-3" type="submit">Submit New Vehicle</button>
        </form>
        <a href="{{route('vehicles.index')}}" class="btn btn-secondary w-100">Cancel</a>
    </div>
</div>


@endsection