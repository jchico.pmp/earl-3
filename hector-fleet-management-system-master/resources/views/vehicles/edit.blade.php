@extends('layouts/app')

@section('title', "Update Vehicle")

@section('content')


<div class="row mt-4">

{{-- Start of Messages --}}
@include('messages')
{{-- End of Messages --}}

    <div class="col-12 col-md-8 col-lg-6 mx-auto text-center">
        
        <div class="row">
            <div class="col-6 text-left mb-5">
                <h2 class="mb-1">Update {{$vehicle->plate_number}}</h2>
            </div>
            <div class="col-6 text-left mb-5 text-right">
                <a href="{{route('vehicles.index')}}" class="btn btn-secondary px-5">Back</a>
            </div>
        </div>

        <img src="/storage/{{$vehicle->vehicle_model->image}}" alt="" class="img-fluid">
        
        <form action="{{route('vehicles.update', ['vehicle' => $vehicle->id])}}" method="post" class="mb-3">
            @method('put')
            @csrf
            
            <div class="form-group text-left">
                <label for="plate_number">Plate Number:</label>
                <input type="text" name="plate_number" class="form-control" value="{{$vehicle->plate_number}}" required>
            </div>
            <div class="form-group text-left mb-5">
                <label for="model">Model:</label>
                <select name="model" class="form-control mb-2" required>
                    <option value="" selected>Choose vehicle model</option>
                    {{-- Start of Model List --}}
                    @foreach($vehicle_models as $vehicle_model)
                    <option value="{{$vehicle_model->id}}"
                        {{$vehicle->vehicle_model_id == $vehicle_model->id ? "selected" : ""}}
                        >
                        {{"$vehicle_model->make $vehicle_model->name"}}
                    </option>
                    @endforeach
                    {{-- End of Model List --}}

                </select>
                <p class="text-center">
                    Vehicle model not on the list? 
                    <a href="{{route('vehicle_models.create')}}" class="btn btn-sm btn-secondary ml-2">Add New Vehicle Model</a>
                </p>
            </div>
            <button class="btn btn-primary px-5" type="submit">Update Vehicle</button>
        </form>

    </div>
</div>


@endsection