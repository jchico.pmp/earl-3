<div class="col-sm-6 p-1
@if(Auth::user() !== null && Auth::user()->can('isMod'))
    col-lg-3
@else
    col-lg-4
@endif
">
    <div class="card">
        <img src="/public/{{$vehicle_model->image}}" alt="" class="img-fluid card-img-top vehicle-img" id="model-{{$vehicle_model->id}}">
        <div class="card-body p-0">
            <div class="col-12">
                <div class="row">
                    <div class="col-12">
                    <h5 class="mb-0">{{"$vehicle_model->make $vehicle_model->name"}}</h5>
                    <small>{{$vehicle_model->year}}</small>
                    </div>
                    <div class="col-12">
                        <p class="mb-0">Category: {{$vehicle_model->vehicle_category->name}} </p>
                        <p>Seats: {{$vehicle_model->seats}}</p>
                    </div>
                    <div class="col-12 px-0">
                        <a href="{{route('vehicle_models.show', ['vehicle_model' => $vehicle_model->id])}}" class="btn btn-primary d-inline-block w-100 text-center py-2">Model Details</a>
                    </div>
        
                    @can('isMod')
                        <div class="col-6 px-0">
                            <a href="{{route('vehicle_models.edit', ['vehicle_model' => $vehicle_model->id])}}" class="btn btn-secondary d-inline-block w-100 py-2">Edit</a>
                        </div>
                        <div class="col-6 px-0">
                            <form action="{{route('vehicle_models.destroy', ['vehicle_model' => $vehicle_model->id])}}" method="post">
                                @csrf
                                @method('delete')
                                <button class="btn btn-danger py-2 w-100">Delete</button>
                            </form>
                        </div>
                    @endcan
        
                    @cannot('isMod')
                    <div class="col-12 px-0">
                        <form action="{{route('order_requests.update', ['order_request' => $vehicle_model->id])}}" method="post">
                            @csrf
                            @method('put')
                            <div class="input-group">
                                <input type="number" name="quantity" class="form-control w-100 text-center card-form" placeholder="Enter Quantity" min="1">
                                <div class="input-group-append">
                                    <button class="btn btn-secondary w-100">Add</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    @endcannot
                </div>
    
            </div>
        </div>
    </div>
</div>