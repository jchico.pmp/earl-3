<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle_status extends Model
{
    public function vehicles() {
        return $this->hasMany(Vehicle::class);
    }
}
