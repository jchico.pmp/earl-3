<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    public function vehicle_model() {
        return $this->belongsTo(Vehicle_model::class);
    }

    public function vehicle_status() {
        return $this->belongsTo(Vehicle_status::class);
    }

            public function orders() {
    	return $this->belongsToMany('App\Order', 'order_vehicle');
    }

}
