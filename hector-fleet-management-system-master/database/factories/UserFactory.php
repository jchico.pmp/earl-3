<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\Department;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

function emailNumber() {
    $result = "";
    for($i = 0; $i < 2; $i++) {
        $result = $result . rand(0,9);
    }

    return $result;
}

$factory->define(User::class, function (Faker $faker) {

    $firstname = $faker->firstname;
    $lastname = $faker->lastname;
    $email = strtolower(substr($firstname, 0, 1) . "." . $lastname) . emailNumber() . "@fleetmanagementsolutions.com";

    return [
        'firstname' => $firstname,
        'lastname' => $lastname,
        'email' => $email,
        'email_verified_at' => now(),
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'role_id' => 3,
        'department_id' => rand(2, count(Department::all())),
        'remember_token' => Str::random(10),
    ];
});
