<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orders')->insert([
            'order_number' => 'ABC1234',
            'borrow_date' => '2020-01-11',
            'return_date' => '2020-01-15',
            'purpose' => 'testing purposes',
            'user_id' => '3',
            'updated_by' => '1',
            'order_status_id' => '1',
            'created_at' => '2020-01-14',
            'updated_at' => '2020-01-14'
        ]);
        DB::table('orders')->insert([
            'order_number' => 'CBD2348',
            'borrow_date' => '2020-02-1',
            'return_date' => '2020-02-5',
            'purpose' => 'testing purposes',
            'user_id' => '3',
            'updated_by' => '1',
            'order_status_id' => '1',
            'created_at' => '2020-01-14',
            'updated_at' => '2020-01-14'
        ]);
        DB::table('orders')->insert([
            'order_number' => 'EFG98874',
            'borrow_date' => '2020-02-21',
            'return_date' => '2020-02-25',
            'purpose' => 'testing purposes',
            'user_id' => '3',
            'updated_by' => '1',
            'order_status_id' => '1',
            'created_at' => '2020-01-14',
            'updated_at' => '2020-01-14'
        ]);
    }
}
