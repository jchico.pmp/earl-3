<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrderStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('order_statuses')->insert([
            'name' => 'Pending'
        ]);
        DB::table('order_statuses')->insert([
            'name' => 'Approved'
        ]);
        DB::table('order_statuses')->insert([
            'name' => 'Completed'
        ]);
        DB::table('order_statuses')->insert([
            'name' => 'Rejected'
        ]);
    }
}
