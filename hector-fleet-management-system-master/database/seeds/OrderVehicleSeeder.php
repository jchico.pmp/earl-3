<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrderVehicleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('order_vehicle')->insert([
            'order_id' => '1',
            'vehicle_id' => '1'
        ]);
        DB::table('order_vehicle')->insert([
            'order_id' => '2',
            'vehicle_id' => '1'
        ]);
        DB::table('order_vehicle')->insert([
            'order_id' => '3',
            'vehicle_id' => '2'
        ]);
    }
}
