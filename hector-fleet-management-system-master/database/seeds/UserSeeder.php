<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'firstname' => 'admin',
            'lastname' => 'admin',
            'email' => 'admin@email.com',
            'password' => Hash::make('password123'),
            'role_id' => '1',
            'department_id' => '1'
        ]);
        DB::table('users')->insert([
            'firstname' => 'inspector',
            'lastname' => 'inspector',
            'email' => 'inspector@email.com',
            'password' => Hash::make('password123'),
            'role_id' => '2',
            'department_id' => '1'
        ]);
        DB::table('users')->insert([
            'firstname' => 'hector',
            'lastname' => 'robles',
            'email' => 'hector@email.com',
            'password' => Hash::make('password123'),
            'role_id' => '3',
            'department_id' => '1'
        ]);
        DB::table('users')->insert([
            'firstname' => 'user',
            'lastname' => 'user',
            'email' => 'user@email.com',
            'password' => Hash::make('password123'),
            'role_id' => '3',
            'department_id' => '1'
        ]);
    }
}
