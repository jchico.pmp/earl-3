<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('departments')->insert([
            'name' => 'Fleet Management'
        ]);
        DB::table('departments')->insert([
            'name' => 'IT'
        ]);
        DB::table('departments')->insert([
            'name' => 'Marketing'
        ]);
        DB::table('departments')->insert([
            'name' => 'Production'
        ]);
        DB::table('departments')->insert([
            'name' => 'Creative'
        ]);
    }
}
