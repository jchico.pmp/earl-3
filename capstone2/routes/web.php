<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::delete('/transactions/empty','TransactionController@empty')->name('transactions.empty');
Route::get('/home', 'HomeController@index')->name('home');

// Clothe Routes
Route::get('/clothes', 'ClotheController@index')->name('clothes.index');
Route::get('/clothes/create', 'ClotheController@create')->name('clothes.create');
Route::get('/clothes/{clothe}/', 'ClotheController@show')->name('clothes.show');
Route::post('/clothes/store', 'ClotheController@store')->name('clothes.store');
Route::get('/clothes/{clothe}/edit', 'ClotheController@edit')->name('clothes.edit');
Route::patch('/clothes/{clothe}/', 'ClotheController@update')->name('clothes.update');
Route::delete('clothes/{clothe}', 'ClotheController@destroy')->name('clothes.destroy');

//Brand Routes
Route::get('/brands', 'BrandController@index')->name('brands.index');
Route::get('/brands/create', 'BrandController@create')->name('brands.create');
Route::post('/brands/store', 'BrandController@store')->name('brands.store');
Route::get('/brands/{brand}/edit', 'BrandController@edit')->name('brands.edit');
Route::patch('/brands/{brand}/', 'BrandController@update')->name('brands.update');
Route::delete('/brands/{brand}', 'BrandController@destroy')->name('brands.destroy');

// Checkout Routes
Route::get('/checkouts', 'CheckoutController@index')->name('checkouts.index');

// Transaction routes
Route::post('/transactions/store', 'TransactionController@store')->name('transactions.store');
Route::get('/transactions/create', 'TransactionController@create')->name('transactions.create');
Route::get('/transactions', 'TransactionController@index')->name('transactions.index');
Route::get('/transactions/{transaction}', 'TransactionController@show')->name('transactions.show');
Route::patch('/transactions/{transaction}', 'TransactionController@update')->name('transactions.update');
Route::delete('/transactions/{transaction}', 'TransactionController@destroy')->name('transactions.destroy');