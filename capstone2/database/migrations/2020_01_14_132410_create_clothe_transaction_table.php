<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClotheTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clothe_transaction', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('transaction_id')->nullable()->default(1);
            $table->foreign('transaction_id')->references('id')->on('transactions')->onDelete('set null')->onUpdate('set null');
            $table->unsignedBigInteger('clothe_id')->nullable()->default(1);
            $table->foreign('clothe_id')->references('id')->on('clothes')->onDelete('set null')->onUpdate('set null');
            $table->integer('quantity');
            $table->float('subtotal');
            $table->float('price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clothing_transactions');
    }
}
