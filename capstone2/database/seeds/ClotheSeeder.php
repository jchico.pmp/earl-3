<?php

use Illuminate\Database\Seeder;

class ClotheSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clothes')->insert([
        	'name' => 'Disney x Gucci Silk Shirt',
        	'price' => '1000.00',
        	'description' => 'A silk shirt designed with style and comfort in mind.',
            'image' => 'public/cyXpBkFqY6qPNETy6XntnVVZQtESWVEmKgi9W19u.jpeg',
            'brand_id' => 1,
            'status_id' => 1,
            'stock_id' => 1
        ]);
        DB::table('clothes')->insert([
        	'name' => 'Nappa Leather Jacket',
        	'price' => '1200.00',
        	'description' => 'A classic jacket made of nappa leather.',
            'image' => 'public/Cz9eArCCXPpasH5cqj53KkOQ5leB5XaFWc6Efr2u.jpeg',
            'brand_id' => 3,
            'status_id' => 1,
            'stock_id' => 2
        ]);
        DB::table('clothes')->insert([
        	'name' => 'Mint Green Track Pants',
        	'price' => '800.00',
        	'description' => 'A pair of green track pants from Virgil.',
            'image' => 'public/ksPgw4QlhEWBmaXPQ22YMQbsuyoKUuKWPZOFa7PS.jpeg',
            'brand_id' => 4,
            'status_id' => 1,
            'stock_id' => 1
        ]);
        DB::table('clothes')->insert([
        	'name' => 'Industrial Trench Coat',
        	'price' => '1400.00',
        	'description' => 'A trench code with industrial vibes.',
            'image' => 'public/NkFkj9p5cqG1zqfR8N6DigRYqV7Hjj5F8JsUQ2O9.jpeg',
            'brand_id' => 4,
            'status_id' => 1,
            'stock_id' => 2
        ]);
        DB::table('clothes')->insert([
        	'name' => 'Nautical Print Bowling Shirt',
        	'price' => '600.00',
        	'description' => 'A bowling shirt with interesting nautical print.',
            'image' => 'public/EMzk5UGzU9ALaQSFnMkrk6qvlZ443hnMmXxUgHTA.jpeg',
            'brand_id' => 1,
            'status_id' => 1,
            'stock_id' => 1
        ]);
        DB::table('clothes')->insert([
        	'name' => 'Orange Shearling Jacket',
        	'price' => '900.00',
        	'description' => 'A sherling jacket in orange color.',
            'image' => 'public/ZKXJpugQoeMmXc34xYiW5uriS62JNaSH8wdSuu6n.jpeg',
            'brand_id' => 4,
            'status_id' => 1,
            'stock_id' => 1
        ]);
    }
}
