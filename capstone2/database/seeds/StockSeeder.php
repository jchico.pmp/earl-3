<?php

use Illuminate\Database\Seeder;

class StockSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('stocks')->insert([
        	'name' => 'Available'
        ]);
        DB::table('stocks')->insert([
        	'name' => 'Not Available'
        ]);
    }
}
