<?php

use Illuminate\Database\Seeder;

class BrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('brands')->insert([
        	'name' => 'Gucci'
        ]);
        DB::table('brands')->insert([
        	'name' => 'Saint Laurent'
        ]);
        DB::table('brands')->insert([
        	'name' => 'Versace'
        ]);
        DB::table('brands')->insert([
        	'name' => 'Off-White'
        ]);
        DB::table('brands')->insert([
        	'name' => 'Supreme'
        ]);
        DB::table('brands')->insert([
        	'name' => 'Dolce and Gabbana'
        ]);
        DB::table('brands')->insert([
        	'name' => 'Bape'
        ]);
        DB::table('brands')->insert([
        	'name' => 'Dior'
        ]);
        DB::table('brands')->insert([
        	'name' => 'Prada'
        ]);
        DB::table('brands')->insert([
        	'name' => 'Burberry'
        ]);
    }
}
