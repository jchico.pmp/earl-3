<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
        	'name' => 'Earl',
        	'email' => 'earlrafael@gmail.com',
        	'password' => Hash::make('awesomeis24'),
            'role_id' => 2
        ]);
        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => Hash::make('admin12345'),
            'role_id' => 1
        ]);
    }
}
