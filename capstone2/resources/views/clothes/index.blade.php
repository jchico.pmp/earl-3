@extends('layouts.app')
@section('content')
<div class="container my-4 pb-5">
	<div class="row">
		<div class="col-12">
			<form action="" method="GET">
				<div class="row">
					<div class="col">
						<select name="brand" id="brand" class="form-control mt-2">
							<option value="">All</option>
							@foreach($brands as $brand)
							<option value="{{$brand->id}}">{{$brand->name}}</option>
							@endforeach
						</select>
					</div>
					<div class="col">
					<button class="btn btn-inverted">Filter</button>	
					</div>
				</div>
			</form>
		</div>
	</div>
	<div class="row mt-3">
		@foreach($clothes as $clothe)
			{{-- start of cards --}}
			<div class="col-12 col-md-4 col-lg-3">
				<div class="card" data-aos="zoom-in">
					<img src="/public/{{$clothe->image}}" alt="" class="card-img-top">
					<div class="card-body">
						<h5 class="card-title">{{$clothe->name}}</h5>
						<p class="card-text">PHP{{number_format($clothe->price,2)}}</p>
						<p class="card-text">{{$clothe->brand->name}}</p>
						@if($clothe->stock->id == 2)
						<p class="card-text"><span class="badge badge-danger align-self-center">{{$clothe->stock->name}}</span></p>
						@else
						<p class="card-text"><span class="badge badge-success align-self-center">{{$clothe->stock->name}}</span></p>
						@endif
						@can('isAdmin')
						<form action="{{route('clothes.update',['clothe' => $clothe->id])}}" method="POST">
							@csrf
							@method('PATCH')
							<select name="stock-id" id="stock-id" class="form-control">
								@foreach($stocks as $stock)
								
								<option value="{{$stock->id}}">{{$stock->name}}</option>
								@endforeach
							</select>
							<button type="submit" class="btn btn-inverted">SET</button>
						</form>
						@endcan
						<p class="card-text">{{$clothe->description}}</p>
					</div>
					<div class="card-footer">
						@cannot('isAdmin')
						<form action="{{route('transactions.update',['transaction' => $clothe->id])}}" method="POST">
							@csrf
							@method('PATCH')
							<input type="number" name="quantity" id="quantity" class="form-control mb-2" min-"1" placeholder="Input Quantity">
							<button class="btn btn-primary w-100 mb-2"{{($stock->name == 'Not Available' ? 'disabled' : '')}}>Rent</button>
							<hr>
						</form>
						@endcannot
						<a href="{{route('clothes.show',['clothe' => $clothe->id])}}" class="btn btn-inverted w-100 mb-2">View Piece</a>
						@can('isAdmin')
						<a href="{{route('clothes.edit',['clothe' => $clothe->id])}}" class="btn btn-custom w-100 mb-2">Edit Piece</a>
						<form action="{{route('clothes.destroy',$clothe)}}" method="POST">
							@csrf
							@method('DELETE')
							<button class="btn btn-delete2 w-100">Delete Piece</button>
						</form>
						@endcan
					</div>
				</div>
			</div>
			{{-- end of cards --}}
		@endforeach
	</div>
</div>
@endsection