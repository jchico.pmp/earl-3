@extends('layouts.app')
@section('content')
<div class="container my-5">
    <div class="row">
        <div class="col col-6 mx-auto">
            <h3>Edit Clothing</h3>
            <hr>
            <form action="{{route('clothes.update',['clothe'=>$clothe->id])}}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PATCH')
                @if($errors->has('name'))
                    <div class="alert alert-danger">
                    Clothing Name required.
                    </div>
                @endif
                <input type="text" name="name" class="form-control mb-3" id="name" placeholder="Clothing Name" value="{{$clothe->name}}">
                @if($errors->has('price'))
                    <div class="alert alert-danger">
                    Clothing Price required.
                    </div>
                @endif
                <input type="text" name="price" class="form-control mb-3" id="price" placeholder="Clothing Price" value="{{$clothe->price}}">
                <select name="brand-id" id="brand-id" class="custom-select mb-3">
                    @foreach($brand as $brand)
                        <option value="{{$brand->id}}" {{old('brand-id') == $brand->id ? 'selected' : ''}}>{{$brand->name}}</option>
                    @endforeach
                </select>
                <select name="stock-id" id="stock-id" class="custom-select mb-3">
                    @foreach($stock as $stock)
                        <option value="{{$stock->id}}" {{old('stock-id') == $stock->id ? 'selected' : ''}}>{{$stock->name}}</option>
                    @endforeach
                </select>

                {{-- @if($errors->has('image'))
                    <div class="alert alert-danger">
                        Product Image required.
                    </div>
                @endif --}}
                <input type="file" name="image" class="form-control-file mb-3" id="image">
                @if($errors->has('description'))
                    <div class="alert alert-danger">
                    Clothing Description required.
                    </div>
                @endif
                <textarea name="description" id="description" cols="10" rows="10" class="form-control mb-3" placeholder="Product Description">{{$clothe->description}}</textarea>
                <button type="submit" class="btn btn-custom">Update Clothing</button>
            </form>
        </div>
        <div class="col col-6">
            {{-- start of cards --}}
               <div class="card">
                   <img src="/public/{{$clothe->image}}" alt="" class="card-img-top">
                   <div class="card-body">
                       <h5 class="card-title">{{$clothe->name}}</h5>
                       <p class="card-text">PHP{{number_format($clothe->price,2)}}</p>
                       <p class="card-text">{{$clothe->brand->name}}</p>
                       <p class="card-text">{{$clothe->stock->name}}</p>
                       <p class="card-text">{{$clothe->description}}</p>
                   </div>
                   <div class="card-footer">
                       <a href="{{route('clothes.index')}}" class="btn btn-custom w-100 mb-2">Show all Clothing</a>
                   </div>
               </div>
           {{-- end of cards --}}
        </div>
    </div>
</div>
@endsection