@extends('layouts.app')
@section('content')
	<ul class="list-unstyled my-3 pb-5">
	@foreach($brand as $brand)
		<li>
			{{ $brand->name }}
			{{-- update --}}
			<a href="{{route('brands.edit',['brand' => $brand->id])}}">Update</a>
			{{-- delete --}}
			<form action="{{route('brands.destroy',['brand' => $brand->id])}}" method='POST'>
				@csrf
				@method('DELETE')
				<button type="submit" class="btn-delete2">Delete</button>
			</form>
		</li>
	@endforeach
	</ul>
@endsection