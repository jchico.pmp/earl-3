@extends('layouts.app')
@section('content')
	<div class="container mt-5">
		<div class="row">
			<div class="col col-12 text-center">	
				<form action="{{route('brands.store')}}" method="POST">
				@csrf
				<label for="name">Brand Name:</label>
				<input type="text" name="name" class="form-control">
				<button type="submit" class="btn-custom mt-4">Add New Brand</button>
				</form>
			</div>
		</div>
	</div>
@endsection