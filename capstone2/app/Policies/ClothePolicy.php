<?php

namespace App\Policies;

use App\Clothe;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ClothePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any clothes.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->role_id == 1;
    }

    /**
     * Determine whether the user can view the clothe.
     *
     * @param  \App\User  $user
     * @param  \App\Clothe  $clothe
     * @return mixed
     */
    public function view(User $user, Clothe $clothe)
    {
        //
    }

    /**
     * Determine whether the user can create clothes.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->role_id == 1;
    }

    /**
     * Determine whether the user can update the clothe.
     *
     * @param  \App\User  $user
     * @param  \App\Clothe  $clothe
     * @return mixed
     */
    public function update(User $user, Clothe $clothe)
    {
        return $user->role_id == 1;
    }

    /**
     * Determine whether the user can delete the clothe.
     *
     * @param  \App\User  $user
     * @param  \App\Clothe  $clothe
     * @return mixed
     */
    public function delete(User $user, Clothe $clothe)
    {
        return $user->role_id == 1;
    }

    /**
     * Determine whether the user can restore the clothe.
     *
     * @param  \App\User  $user
     * @param  \App\Clothe  $clothe
     * @return mixed
     */
    public function restore(User $user, Clothe $clothe)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the clothe.
     *
     * @param  \App\User  $user
     * @param  \App\Clothe  $clothe
     * @return mixed
     */
    public function forceDelete(User $user, Clothe $clothe)
    {
        //
    }
}
