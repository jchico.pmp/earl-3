<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clothe extends Model
{
    public function brand()
    {
    	return $this->belongsTo('App\Brand');
    }
    public function stock()
    {
    	return $this->belongsTo('App\Stock');
    }
}
